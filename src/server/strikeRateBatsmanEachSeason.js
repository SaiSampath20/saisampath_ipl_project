function strikeRateBatsmanEachSeason(matchesObject, deliveriesObject) {
    let seasonMatchIds = {};
    let batsmanRunsEachSeason = {};

    matchesObject.forEach( (matchObject) => {
        if( seasonMatchIds.hasOwnProperty( matchObject.season ) ) {
            let tempArray = seasonMatchIds[matchObject.season];
            tempArray[1] = parseInt(matchObject.id);
            seasonMatchIds[matchObject.season] = tempArray;
        } else {
            let tempArray = [parseInt(matchObject.id), 1];
            seasonMatchIds[matchObject.season] = tempArray;
        }
        
    });

    // console.log( seasonMatchIds );

    deliveriesObject.forEach( (deliveryObject) => {

        let season;
        
        for( const [key, value] of Object.entries(seasonMatchIds) ) {
            if( deliveryObject.match_id >= value[0] && deliveryObject.match_id <= value[1] ) {
                season = parseInt( key );
                break;
            }
        }

        if( batsmanRunsEachSeason.hasOwnProperty( deliveryObject.batsman ) ) {
            let eachSeason = batsmanRunsEachSeason[deliveryObject.batsman];
            if( eachSeason.hasOwnProperty( season ) ) {
                let tempArray = eachSeason[season];
                tempArray[0] += parseInt(deliveryObject.batsman_runs);
                tempArray[1] += 1;
                eachSeason[season] = tempArray;
                batsmanRunsEachSeason[deliveryObject.batsman] = eachSeason;
            } else {
                let batsmanRunsAndBalls = [parseInt(deliveryObject.batsman_runs), 1];
                let eachSeason = {};
                eachSeason[season] = batsmanRunsAndBalls;
                batsmanRunsEachSeason[deliveryObject.batsman] = eachSeason;
            }
        } else {
            let batsmanRunsAndBalls = [parseInt(deliveryObject.batsman_runs), 1];
            let eachSeason = {};
            eachSeason[season] = batsmanRunsAndBalls;
            batsmanRunsEachSeason[deliveryObject.batsman] = eachSeason;
        }
        
        // if( batsmanRunsEachSeason.hasOwnProperty( season ) ) {
        //     let batsman = batsmanRunsEachSeason[season];
        //     console.log( batsman );
        //     for(const [key, value] in Object.entries(batsman) ) {
        //         if( key === deliveryObject.batsman ) {
        //             let tempArray = value;
        //             tempArray[0] += parseInt(deliveryObject.batsman_runs);
        //             tempArray[1] += 1;
        //             batsman[deliveryObject.batsman] = tempArray;
        //             batsmanRunsEachSeason[season] = batsman;
        //         } else {
        //             let batsmanRunsAndBalls = [parseInt(deliveryObject.batsman_runs), 1];
        //             let batsman = {};
        //             batsman[deliveryObject.batsman] = batsmanRunsAndBalls;
        //             batsmanRunsEachSeason[season] = batsman;
        //         }
        //     }
        // } else {
        //     let batsmanRunsAndBalls = [parseInt(deliveryObject.batsman_runs), 1];
        //     let batsman = {};
        //     batsman[deliveryObject.batsman] = batsmanRunsAndBalls;
        //     batsmanRunsEachSeason[season] = batsman;
        //     // console.log( batsmanRunsEachSeason );
        // }

    });
    
    console.log( batsmanRunsEachSeason );

    return null;
}

module.exports = strikeRateBatsmanEachSeason;