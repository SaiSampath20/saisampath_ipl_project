function highestPlayerOfMatchEachSeason(matchesObject) {
    let playerOfMatchEachSeasonObject = {};
    let highestPlayerOfMatchEachSeasonObject = {};

    matchesObject.forEach( (matchObject) => {
        
        if( playerOfMatchEachSeasonObject.hasOwnProperty( matchObject.season ) ) {
            let playerOfMatch = playerOfMatchEachSeasonObject[matchObject.season];
            if( playerOfMatch.hasOwnProperty( matchObject.player_of_match ) ) {
                playerOfMatch[matchObject.player_of_match] += 1;
            } else {
                playerOfMatch[matchObject.player_of_match] = 1;
            }
        } else {
            let playerOfMatch = {};
            playerOfMatch[ matchObject.player_of_match ] = 1;
            playerOfMatchEachSeasonObject[matchObject.season] = playerOfMatch;
        }

    });

    for (const [key, value] of Object.entries(playerOfMatchEachSeasonObject)) {
        let tempArray = ['', 0];
        for(const [key1, value1] of Object.entries(value)) {
            if( tempArray[1] < value1 ){
                tempArray[1] = value1;
                tempArray[0] = key1;
            }    
        }
        highestPlayerOfMatchEachSeasonObject[key] = tempArray[0];
    }

    return highestPlayerOfMatchEachSeasonObject;
}

module.exports = highestPlayerOfMatchEachSeason;