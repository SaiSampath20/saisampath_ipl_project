function boundariesByEachBatsman2012(matchesObject, deliveriesObject) {
    let ids2012 = [];
    let batsmanBoundaries2012 = {};

    matchesObject.forEach( (matchObject) => {
        if( matchObject.season == "2012" )
            ids2012.push( matchObject.id );
    });

    deliveriesObject.forEach( (deliveryObject) => {

        if( ids2012.indexOf( deliveryObject.match_id ) > -1 ){
            // console.log( deliveryObject.match_id );
            if( deliveryObject.batsman_runs == '4' || deliveryObject.batsman_runs == '6' ){
                if( deliveryObject.batsman in batsmanBoundaries2012 ) {
                    batsmanBoundaries2012[ deliveryObject.batsman ] += 1;
                } else {
                    batsmanBoundaries2012[ deliveryObject.batsman ] = 1;
                }
            }    
        }

    });
    
    return batsmanBoundaries2012;
}

module.exports = boundariesByEachBatsman2012;