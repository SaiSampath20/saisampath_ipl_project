function matchesPlayedPerYear(matchesObject, deliveriesObject) {
    let matchesPlayedPerYearObject = {};

    matchesObject.forEach( (matchObject) => {

        if (matchObject.season in matchesPlayedPerYearObject) {
            matchesPlayedPerYearObject[ matchObject.season ] += 1;
        }
        else {
            matchesPlayedPerYearObject[ matchObject.season ] = 1;
        }

    });
    
    return matchesPlayedPerYearObject;
}

module.exports = matchesPlayedPerYear;