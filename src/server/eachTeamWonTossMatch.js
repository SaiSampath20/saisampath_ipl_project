function eachTeamWonTossAndMatch(matchesObject) {
    let eachTeamWonTossAndMatch = {};

    matchesObject.forEach( (matchObject) => {

        if( eachTeamWonTossAndMatch.hasOwnProperty( matchObject.team1 ) && 
            eachTeamWonTossAndMatch.hasOwnProperty( matchObject.team2 ) ) {
            
            let team1Object = eachTeamWonTossAndMatch[matchObject.team1];
            let team2Object = eachTeamWonTossAndMatch[matchObject.team2];

            if( matchObject.team1 == matchObject.toss_winner &&
                matchObject.team1 == matchObject.winner ) {
                team1Object.toss_winner += 1;
                team1Object.winner += 1;
            } else if( matchObject.team1 == matchObject.toss_winner ) {
                team1Object.toss_winner += 1;
            } else if( matchObject.team1 == matchObject.winner ) {
                team1Object.winner += 1;
            }

            if( matchObject.team2 == matchObject.toss_winner &&
                team2Object.winner == matchObject.winner ) {
                team2Object.toss_winner += 1;
                team2Object.winner += 1;
            } else if( matchObject.team2 == matchObject.toss_winner ) {
                team2Object.toss_winner += 1;
            } else if( matchObject.team2 == matchObject.winner ) {
                team2Object.winner += 1;
            }

            eachTeamWonTossAndMatch[matchObject.team1] = team1Object;
            eachTeamWonTossAndMatch[matchObject.team2] = team2Object;
 
        } else {
            let team1Object = {};
            let team2Object = {};

            // initializing the team1Object with toss_winner count and winner count 
            if( matchObject.team1 == matchObject.toss_winner && matchObject.team1 == matchObject.winner ) {
                team1Object["toss_winner"] = 1;
                team1Object["winner"] = 1;
            } else if( matchObject.team1 == matchObject.toss_winner ) {
                team1Object["toss_winner"] = 1;
                team1Object["winner"] = 0;
            } else if( matchObject.team1 == matchObject.winner ) {
                team1Object["toss_winner"] = 0;
                team1Object["winner"] = 1;
            } else {
                team1Object["toss_winner"] = 0;
                team1Object["winner"] = 0;
            }

            // initializing the team2Object with toss_winner count and winner count
            if( matchObject.team2 == matchObject.toss_winner && matchObject.team2 == matchObject.winner ) {
                team2Object["toss_winner"] = 1;
                team2Object["winner"] = 1;
            } else if( matchObject.team2 == matchObject.toss_winner ) {
                team2Object["toss_winner"] = 1;
                team2Object["winner"] = 0;
            } else if( matchObject.team2 == matchObject.winner ) {
                team2Object["toss_winner"] = 0;
                team2Object["winner"] = 1;
            } else {
                team2Object["toss_winner"] = 0;
                team2Object["winner"] = 0;
            }

            eachTeamWonTossAndMatch[matchObject.team1] = team1Object;
            eachTeamWonTossAndMatch[matchObject.team2] = team2Object;
        }

    });

    return eachTeamWonTossAndMatch;
}

module.exports = eachTeamWonTossAndMatch;