const fs = require('fs');
const matchesPlayedPerYear = require('./matchesPlayedPerYear');
const matchesWonPerTeamPerYear = require('./matchesWonPerTeamPerYear');
const extraRunsPerTeam2016 = require('./extraRunsPerTeam2016');
const top10EconomicalBowlers2015 = require('./top10EconomicalBowlers2015');
const writeJsonObjectToJson = require('./writeJsonObjectToJson');
const boundariesByEachBatsman2012 = require('./boundariesByEachBatsman2012');
const eachTeamWonTossAndMatch = require('./eachTeamWonTossMatch');
const highestPlayerOfMatchEachSeason = require('./highestPlayerOfMatchEachSeason');
const strikeRateBatsmanEachSeason = require('./strikeRateBatsmanEachSeason');
const bowlerEconomySuperOvers = require('./bowlerEconomySuperOvers');
const highestNoOfTimesOnePlayerDismissedAnotherPlayer = require('./highestNoOfTimesOnePlayerDismissedAnotherPlayer');

// let obj = csvToJson.fieldDelimiter(',').getJsonFromCsv('./data/matches.csv');
let matchesObject = JSON.parse( fs.readFileSync('src/data/matches.json' , 'utf8' ) );
let deliveriesObject = JSON.parse( fs.readFileSync('src/data/deliveries.json', 'utf-8') );

const matchesPlayedPerYearObject = matchesPlayedPerYear(matchesObject, deliveriesObject);
const matchesWonPerTeamPerYearObject = matchesWonPerTeamPerYear(matchesObject, deliveriesObject);
const extraRunsPerTeam2016Object = extraRunsPerTeam2016(matchesObject, deliveriesObject);
const top10EconomicalBowlers2015Object = top10EconomicalBowlers2015(matchesObject, deliveriesObject);
const boundariesByEachBatsman2012Object = boundariesByEachBatsman2012(matchesObject, deliveriesObject);
const eachTeamWonTossAndMatchObject = eachTeamWonTossAndMatch(matchesObject);
const highestPlayerOfMatchEachSeasonObject = highestPlayerOfMatchEachSeason(matchesObject);
const strikeRateBatsmanEachSeasonObject = strikeRateBatsmanEachSeason(matchesObject, deliveriesObject);
const bowlerEconomySuperOversObject = bowlerEconomySuperOvers(deliveriesObject);
const highestNoOfTimesOnePlayerDismissedAnotherPlayerObject = highestNoOfTimesOnePlayerDismissedAnotherPlayer(deliveriesObject);

writeJsonObjectToJson("./src/public/output/matchesPlayedPerYear.json", matchesPlayedPerYearObject );
writeJsonObjectToJson("./src/public/output/matchesWonPerTeamPerYear.json", matchesWonPerTeamPerYearObject );
writeJsonObjectToJson("./src/public/output/extraRunsPerTeam2016.json", extraRunsPerTeam2016Object);
writeJsonObjectToJson("./src/public/output/top10EconomicalBowlers2015.json", top10EconomicalBowlers2015Object);
writeJsonObjectToJson("./src/public/output/boundariesByEachBatsman2012.json", boundariesByEachBatsman2012Object);
writeJsonObjectToJson("./src/public/output/eachTeamWonTossAndMatch.json", eachTeamWonTossAndMatchObject);
writeJsonObjectToJson("./src/public/output/highestPlayerOfMatchEachSeason.json", highestPlayerOfMatchEachSeasonObject);
writeJsonObjectToJson("./src/public/output/strikeRateBatsmanEachSeason.json", strikeRateBatsmanEachSeasonObject);
writeJsonObjectToJson("./src/public/output/bowlerEconomySuperOvers.json", bowlerEconomySuperOversObject);
writeJsonObjectToJson("./src/public/output/highestNoOfTimesOnePlayerDismissedAnotherPlayer.json", highestNoOfTimesOnePlayerDismissedAnotherPlayerObject);