const http = require("http");
const fs = require("fs");
const path = require("path");

const server = http.createServer((req, res) => {
    // console.log(urlCopy);

    try {
        let reqURLArr = req.url.split("/");
        if (reqURLArr.length > 2) {
            throw new Error();
        }
        let extension = reqURLArr[reqURLArr.length - 1].split(".");
        // extension[extension.length - 1] === "json"

        if (reqURLArr[1] === "ipl") {
            fs.readFile(
                path.join(__dirname, "../public/index.html"),
                "utf-8",
                (err, data) => {
                    if (err) {
                        res.writeHead(400, {
                            "Content-Type": "application/json",
                        });
                        res.end(`<p> File Not found </p>`);
                    } else {
                        res.writeHead(200, { "Content-Type": "text/html" });
                        res.end(data);
                    }
                }
            );
        } else if (reqURLArr[1] === "ipl.js") {
            fs.readFile(
                path.join(__dirname, "../public/ipl.js"),
                "utf-8",
                (err, data) => {
                    if (err) {
                        res.writeHead(400, {
                            "Content-Type": "application/json",
                        });
                        res.end(`<p> File Not found </p>`);
                    } else {
                        res.writeHead(200, {
                            "Content-Type": "application/javascript",
                        });
                        res.end(data);
                    }
                }
            );
        } else if (extension[extension.length - 1] === "json") {
            const Jsonpath = path.join(__dirname, "../public/output", reqURLArr[1]);

            fs.readFile(Jsonpath, "utf-8", (err, data) => {
                if (err) {
                    res.writeHead(400, { "Content-Type": "text/html" });
                    res.end(`<p> File Not found </p>`);
                } else {
                    res.writeHead(200, { "Content-Type": "application/json" });
                    res.end(data);
                }
            });
        } else {
            res.writeHead(400, { "Content-Type": "text/html" });
            res.end(`<p> File Not found </p>`);
        }
    } catch (error) {
        console.log(error.message);
        res.writeHead(400, { "Content-Type": "application/json" });
        res.end(`<p> File Not found </p>`);
    }
});

server.listen(process.env.PORT || 3000, (err) => {
    if (err) {
        console.log(err.message);
    } else {
        console.log("Listening");
    }
});