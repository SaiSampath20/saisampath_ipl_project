function highestNoOfTimesOnePlayerDismissedAnotherPlayer(deliveriesObject) {
    let onePlayerDismissedAnotherPlayer = {};


    deliveriesObject.forEach( (deliveryObject) => {
        if( deliveryObject.player_dismissed.length != 0 &&  deliveryObject.dismissal_kind != "run out" ) { 
            let uniqueKey = deliveryObject.bowler + "_" + deliveryObject.player_dismissed ;
            if( onePlayerDismissedAnotherPlayer.hasOwnProperty( uniqueKey ) ) {
                onePlayerDismissedAnotherPlayer[uniqueKey] += 1;
            } else {
                onePlayerDismissedAnotherPlayer[uniqueKey] = 1;
            }
        }
    });

    var onePlayerDismissedAnotherPlayerArray = [];
    for (var player in onePlayerDismissedAnotherPlayer) {
        let bowlerBatsman = player.split("_");
        onePlayerDismissedAnotherPlayerArray.push([bowlerBatsman[0], bowlerBatsman[1], onePlayerDismissedAnotherPlayer[player]]);
    }

    onePlayerDismissedAnotherPlayerArray.sort(function(a, b) {
        return b[2] - a[2];
    });

    return onePlayerDismissedAnotherPlayerArray[0];
}

module.exports = highestNoOfTimesOnePlayerDismissedAnotherPlayer;