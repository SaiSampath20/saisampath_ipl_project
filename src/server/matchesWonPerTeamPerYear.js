function matchesWonPerTeamPerYear(matchesObject, deliveriesObject){
    let matchesWonPerTeamPerYearObject = {};

    matchesObject.forEach( (matchObject) => {
        
        if(matchObject.result == "normal") {
            
            if (matchObject.season in matchesWonPerTeamPerYearObject) {
                let getJsonObject = matchesWonPerTeamPerYearObject[matchObject.season];
                if (matchObject.winner in getJsonObject) {
                    getJsonObject[matchObject.winner] = getJsonObject[matchObject.winner] + 1;
                }
                else {
                    getJsonObject[matchObject.winner] = 1;
                }
                matchesWonPerTeamPerYearObject[matchObject.season] = getJsonObject;
            }
            else {
                let tempJsonObject = {};
                tempJsonObject[matchObject.winner] = 1;
                matchesWonPerTeamPerYearObject[matchObject.season] = tempJsonObject;
            }

        }

    } );

    return matchesWonPerTeamPerYearObject;
}

module.exports = matchesWonPerTeamPerYear;