function bowlerEconomySuperOvers(deliveriesObject) {
    let bowlerEconomySuperOver = {};

    deliveriesObject.forEach( (deliveryObject) => {

        if( deliveryObject.is_super_over == 1 ) {

            let runsPerBall = parseInt(deliveryObject.wide_runs) + parseInt(deliveryObject.noball_runs) + parseInt(deliveryObject.batsman_runs);

            if( Object.keys(bowlerEconomySuperOver).indexOf(deliveryObject.bowler) > -1 ){

                if( deliveryObject.ball == 6 ){
                    bowlerEconomySuperOver[deliveryObject.bowler][0] += 1;
                    bowlerEconomySuperOver[deliveryObject.bowler][1] = 0;
                } else if( deliveryObject.ball < 6 ) {
                    bowlerEconomySuperOver[deliveryObject.bowler][1] += 1;
                }
                bowlerEconomySuperOver[deliveryObject.bowler][2] += runsPerBall;

            } else {
                    
                let tempArray = [0, 1, runsPerBall];

                bowlerEconomySuperOver[deliveryObject.bowler] = tempArray;

            }

        }

    });

    let bowlerEconomySuperOverArray = [];

    for (key in bowlerEconomySuperOver) {
        let getArray = bowlerEconomySuperOver[key];
        let balls = getArray[1] / 6;
        let economyRate;
        if( getArray[0] == 0 ) {
            economyRate = getArray[2] / balls;
        } else {
            economyRate = getArray[2] / getArray[0];
        } 
        bowlerEconomySuperOverArray.push([key, economyRate]);
    }

    // console.log( bowlerEconomySuperOverArray );

    return bowlerEconomySuperOverArray;
}

module.exports = bowlerEconomySuperOvers;