function top10EconomicalBowlers2015(matchesObject, deliveriesObject) {
    let top10EconomicalBowlers2015Object = {};
    let matchId2015 = [];

    matchesObject.filter( (matchObject) => {
        if( matchObject.season == "2015" )
            matchId2015.push( matchObject.id );
    });

    deliveriesObject.forEach( (deliveryObject) => {

        if( matchId2015.indexOf(deliveryObject.match_id) > -1 ){
            
            let runsPerBall = parseInt(deliveryObject.wide_runs) + parseInt(deliveryObject.noball_runs) + parseInt(deliveryObject.batsman_runs);

            if( Object.keys(top10EconomicalBowlers2015Object).indexOf(deliveryObject.bowler) > -1 ){

                if( deliveryObject.ball == 6 ){
                    top10EconomicalBowlers2015Object[deliveryObject.bowler][0] += 1;
                    top10EconomicalBowlers2015Object[deliveryObject.bowler][1] = 0;
                } else {
                    top10EconomicalBowlers2015Object[deliveryObject.bowler][1] += 1;
                }
                top10EconomicalBowlers2015Object[deliveryObject.bowler][2] += runsPerBall;

            } else {
                    
                let tempArray = [0, 1, runsPerBall];

                top10EconomicalBowlers2015Object[deliveryObject.bowler] = tempArray;

            }
        }

    });

    let top10EconomicalBowlers2015Array = [];

    for (key in top10EconomicalBowlers2015Object) {
        let getArray = top10EconomicalBowlers2015Object[key];
        let economyRate = getArray[2] / getArray[0];
        top10EconomicalBowlers2015Array.push([key, economyRate]);
    }

    top10EconomicalBowlers2015Array.sort(function (a, b) {
        return a[1] - b[1];
    });

    const top10Bowlers = top10EconomicalBowlers2015Array.slice(0, 10);

    return top10Bowlers;
}

module.exports = top10EconomicalBowlers2015;