function extraRunsPerTeam2016(matchesObject, deliveriesObject) {
    let extraRunsPerTeam2016Object = {};
    let matchId2016 = [];

    matchesObject.filter( (matchObject) => {
        if( matchObject.season == "2016" )
            matchId2016.push( matchObject.id );
    });

    deliveriesObject.forEach( (deliveryObject) => {

        if (matchId2016.indexOf(deliveryObject.match_id) > -1) {
            
            if (Object.keys(extraRunsPerTeam2016Object).indexOf(deliveryObject.bowling_team) > -1) {
                
                extraRunsPerTeam2016Object[deliveryObject.bowling_team] += parseInt(deliveryObject.extra_runs);
            }
            else {
                extraRunsPerTeam2016Object[deliveryObject.bowling_team] = parseInt(deliveryObject.extra_runs);
            }
            
        }     
    });

    return extraRunsPerTeam2016Object;
}

module.exports = extraRunsPerTeam2016;