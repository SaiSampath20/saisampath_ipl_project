const csvToJson = require('convert-csv-to-json');
const path = require('path');

let matchesCsv = './data/matches.csv'; 
let matchesJson = './data/matches.json';

let deliveriesCsv = './data/deliveries.csv';
let deliveriesJson = './data/deliveries.json';

let getMatchesCsv = path.join(__dirname, matchesCsv);
let getMatchesJson = path.join(__dirname, matchesJson );

let getDeliveriesCsv = path.join(__dirname, deliveriesCsv);
let getDeliveriesJson = path.join(__dirname, deliveriesJson);

csvToJson.fieldDelimiter(',').generateJsonFileFromCsv(getMatchesCsv, getMatchesJson);
csvToJson.fieldDelimiter(',').generateJsonFileFromCsv(getDeliveriesCsv, getDeliveriesJson);

