// import fetch from 'node-fetch';

function cylinderHighChart(id, data) {
    if (!Array.isArray(data) || typeof id !== "string") {
        throw new Error();
    } else {
        // Set up the chart
        Highcharts.chart(id, {
            chart: {
                type: "cylinder",
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25,
                },
            },
            title: {
                text: "Highcharts Cylinder Chart",
            },
            plotOptions: {
                series: {
                    depth: 25,
                    colorByPoint: true,
                },
            },
            series: [{
                data: data,
                name: "Cylinders",
                showInLegend: false,
            }, ],
        });
    }
}

// question 1
function matchesPlayedPerYear() {
    fetch("./matchesPlayedPerYear.json")
        .then((response) => response.json())
        .then((data) => {
            cylinderHighChart("matches-played-per-year", Object.entries(data));
        })
        .catch((err) => console.log(err.message));
}

function boundariesByEachBatsman2012HighChart(id, data) {
    if (!Array.isArray(data) || typeof id !== "string") {
        throw new Error();
    } else {
        // Set up the chart
        const chart = new Highcharts.Chart({
            chart: {
                renderTo: id,
                type: "column",
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25,
                },
            },
            title: {},
            subtitle: {},
            plotOptions: {
                column: {
                    depth: 25,
                },
            },
            series: [{
                data: data,
            }, ],
        });

        function showValues() {
            document.getElementById("alpha-value").innerHTML =
                chart.options.chart.options3d.alpha;
            document.getElementById("beta-value").innerHTML =
                chart.options.chart.options3d.beta;
            document.getElementById("depth-value").innerHTML =
                chart.options.chart.options3d.depth;
        }

        // Activate the sliders
        document.querySelectorAll("#sliders input").forEach((input) =>
            input.addEventListener("input", (e) => {
                chart.options.chart.options3d[e.target.id] = parseFloat(
                    e.target.value
                );
                showValues();
                chart.redraw(false);
            })
        );

        showValues();
    }
}

// question 2
function boundariesByEachBatsman2012() {
    fetch("./boundariesByEachBatsman2012.json")
        .then((res) => res.json())
        .then((data) => {
            boundariesByEachBatsman2012HighChart(
                "bounderies2012",
                Object.entries(data)
            );
        })
        .catch((err) => err.message);
}

function bowlerEconomySuperOversHighChart(id, data) {
    if (!Array.isArray(data) || typeof id !== "string") {
        throw new Error();
    } else {
        // Set up the chart
        Highcharts.chart(id, {
            chart: {
                type: "column",
            },
            title: {},
            subtitle: {},
            xAxis: {
                type: "category",
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: "13px",
                        fontFamily: "Verdana, sans-serif",
                    },
                },
            },
            yAxis: {
                min: 0,
                title: {
                    text: "Economy Rate",
                },
            },
            legend: {
                enabled: false,
            },
            tooltip: {
                pointFormat: "Economy Rate: <b>{point.y:.1f}</b>",
            },
            series: [{
                name: "EconomyRate",
                data: data,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: "#FFFFFF",
                    align: "right",
                    format: "{point.y:.1f}", // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: "13px",
                        fontFamily: "Verdana, sans-serif",
                    },
                },
            }, ],
        });
    }
}

// question 3
function bowlerEconomySuperOvers() {
    fetch("./bowlerEconomySuperOvers.json")
        .then((res) => res.json())
        .then((data) => {
            bowlerEconomySuperOversHighChart("EconomySuperOvers", data);
        })
        .catch((err) => console.log(err.message));
}

// question 4
function extraRunsPerTeam2016() {
    fetch("./extraRunsPerTeam2016.json")
        .then((res) => res.json())
        .then((data) => {
            cylinderHighChart("extraRunsPerTeam2016", Object.entries(data));
        })
        .catch((err) => console.log(err.message));
}

function top10EconomicalBowlers2015HighChart(id, bowlerNames, economyRate) {
    if (!Array.isArray(bowlerNames) ||
        !Array.isArray(economyRate) ||
        typeof id !== "string"
    ) {
        throw new Error();
    } else {
        // Set up the chart
        const chart = Highcharts.chart(id, {
            title: {
                text: "Chart.update",
            },
            subtitle: {
                text: "Plain",
            },
            xAxis: {
                categories: bowlerNames,
            },
            series: [{
                type: "column",
                colorByPoint: true,
                data: economyRate,
                showInLegend: false,
            }, ],
        });

        document.getElementById("plain").addEventListener("click", () => {
            chart.update({
                chart: {
                    inverted: false,
                    polar: false,
                },
                subtitle: {},
            });
        });

        document.getElementById("inverted").addEventListener("click", () => {
            chart.update({
                chart: {
                    inverted: true,
                    polar: false,
                },
                subtitle: {
                    text: "Inverted",
                },
            });
        });

        document.getElementById("polar").addEventListener("click", () => {
            chart.update({
                chart: {
                    inverted: false,
                    polar: true,
                },
                subtitle: {
                    text: "Polar",
                },
            });
        });
    }
}

// question 5
function top10EconomicalBowlers2015() {
    fetch("./top10EconomicalBowlers2015.json")
        .then((res) => res.json())
        .then((data) => {
            let bowlerNames = [];
            let economyRate = [];

            data.forEach((element) => {
                bowlerNames.push(element[0]);
                economyRate.push(element[1]);
            });

            top10EconomicalBowlers2015HighChart(
                "top10EconomicalBowlers2015",
                bowlerNames,
                economyRate
            );
        })
        .catch((err) => console.log(err.message));
}

function eachTeamWonTossAndMatchHighChart(id, data) {
    if (!Array.isArray(data) || typeof id !== "string") {
        throw new Error();
    } else {
        Highcharts.chart(id, {
            chart: {
                type: "column",
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    viewDistance: 25,
                    depth: 40,
                },
            },

            title: {},

            xAxis: {
                categories: ["toss_winner", "winner"],
                labels: {
                    skew3d: true,
                    style: {
                        fontSize: "16px",
                    },
                },
            },

            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: "Teams",
                    skew3d: true,
                },
            },

            tooltip: {
                headerFormat: "<b>{point.key}</b><br>",
                pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}',
            },

            plotOptions: {
                column: {
                    stacking: "normal",
                    depth: 40,
                },
            },

            series: data,
        });
    }
}

// question 6
function eachTeamWonTossAndMatch() {
    fetch("./eachTeamWonTossAndMatch.json")
        .then((res) => res.json())
        .then((data) => {
            let array = [];

            Object.entries(data).forEach((obj) => {
                let object = {};
                object["name"] = obj[0];
                let tempArray = [obj[1]["toss_winner"], obj[1]["winner"]];
                object["data"] = tempArray;
                array.push(object);
            });

            eachTeamWonTossAndMatchHighChart("eachTeamWonTossAndMatch", array);
        })
        .catch((err) => console.log(err.message));
}

matchesPlayedPerYear();
boundariesByEachBatsman2012();
bowlerEconomySuperOvers();
extraRunsPerTeam2016();
top10EconomicalBowlers2015();
eachTeamWonTossAndMatch();